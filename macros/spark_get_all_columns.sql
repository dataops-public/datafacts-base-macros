{% macro get_all_columns() %}

{%- set custom_schema = generate_schema_name() -%}

{%- call statement('useSchema', fetch_result=false) -%}
    use {{custom_schema}}
{%- endcall -%}

{%- call statement('result', fetch_result=True) -%}
    show tables
{%- endcall -%}

{% if execute %}
    {%- set table_list = load_result('result')['data'] -%}
    {%- if table_list -%}
            {%- for tab in table_list -%}
                {%- call statement('get_table_columns', fetch_result=True) %}
                    describe {{tab[1]}}
                {%- endcall -%}

                {%- set table_columns = load_result('get_table_columns')['data'] -%}
                {%- if table_columns -%}
                        {%- for column in table_columns -%}
                            select '{{tab[1]}}' as tableName,'{{column[0]}}' as col_name,'{{column[1]}}' as data_type,'{{column[2]}}' as comment
                            {% if not loop.last %}
                                UNION ALL
                            {% endif %}
                        {%- endfor -%}
                {% endif %}

                {% if not loop.last %}
                    UNION ALL
                {% endif %}

            {%- endfor -%}
    {%- else -%}
        {{ return([]) }}
    {%- endif -%}
   
{% endif %}

{% endmacro %}