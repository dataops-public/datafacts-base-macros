{% macro default__drop_all_schemas(schemas_to_drop) %}
    {% for schemaName in schemas_to_drop %}
        {%- if target.database|length -%}
            {% set relation = api.Relation.create(database=target.database, schema=schemaName) %}
        {%- else -%}
            {% set relation = api.Relation.create(database=none, schema=schemaName) %}
        {%- endif -%}
        {% do adapter.drop_schema(relation) %}
    {% endfor %}
{% endmacro %}