{% macro get_all_tables() %}

{%- set custom_schema = generate_schema_name() -%}

{%- call statement('useSchema', fetch_result=false) -%}
    use {{custom_schema}}
{%- endcall -%}

{%- call statement('result', fetch_result=True) -%}
    show tables
{%- endcall -%}

{% if execute %}
    {%- set table_list = load_result('result')['data'] -%}
    {%- if table_list -%}
            {%- for tab in table_list -%}
                select '{{tab[0]}}' as database,'{{tab[1]}}' as tableName,'{{tab[2]}}' as isTemporary
                {% if not loop.last %}
                    UNION ALL
                {% endif %}
            {%- endfor -%}
    {%- else -%}
        {{ return([]) }}
    {%- endif -%}
   
{% endif %}

{% endmacro %}