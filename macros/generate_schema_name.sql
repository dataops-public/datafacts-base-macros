{% macro generate_schema_name(custom_schema_name, node) -%}

    {%- set default_schema = target.schema -%}

    {%- set workspace_schema -%}
        {{ env_var('custom_schema_name','') }}
    {%- endset -%}

    {%- if workspace_schema|length -%}
       {{ default_schema }}_{{ workspace_schema | trim }}
    {%- else -%}
       {{ default_schema }}
    {%- endif -%}

{%- endmacro %}