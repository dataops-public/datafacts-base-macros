{% macro drop_all_schemas(schema_name) %}
    {% set schemas_to_drop = get_schemas_used(schema_name) %}
    {{ adapter.dispatch('drop_all_schemas')(schemas_to_drop) }}
{% endmacro %}