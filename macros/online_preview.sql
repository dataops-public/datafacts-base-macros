{% macro online_preview(sql,path,limit) %}
    {%- if not limit -%}
        {% set limit = 100 %}
    {%- endif -%}
    {% set results = run_query("select * from ( "~sql~" ) as tmp limit "~limit) %}
    {{results.to_json(path)}}
{% endmacro %}